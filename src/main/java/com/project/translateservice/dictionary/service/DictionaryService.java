package com.project.translateservice.dictionary.service;

import com.project.translateservice.dictionary.model.Dictionary;

import java.util.List;

public interface DictionaryService {

    List<Dictionary> getSavedWords();

    void saveWord(String word, String translation);

}
