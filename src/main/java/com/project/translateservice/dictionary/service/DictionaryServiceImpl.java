package com.project.translateservice.dictionary.service;

import com.project.translateservice.dictionary.model.Dictionary;
import com.project.translateservice.dictionary.repository.DictionaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DictionaryServiceImpl implements DictionaryService {

    private final DictionaryRepository dictionaryRepository;

    @Autowired
    public DictionaryServiceImpl(DictionaryRepository dictionaryRepository) {
        this.dictionaryRepository = dictionaryRepository;
    }

    @Override
    public List<Dictionary> getSavedWords() {
        return dictionaryRepository.getSavedWords();
    }

    @Override
    public void saveWord(String word, String translation) {
        dictionaryRepository.saveWord(word, translation);
    }
}
