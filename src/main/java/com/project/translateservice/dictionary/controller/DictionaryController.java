package com.project.translateservice.dictionary.controller;

import com.project.translateservice.dictionary.model.Dictionary;
import com.project.translateservice.dictionary.service.DictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("dictionary")
public class DictionaryController {

    private final DictionaryService dictionaryService;

    @Autowired
    public DictionaryController(DictionaryService dictionaryService) {
        this.dictionaryService = dictionaryService;
    }

    @GetMapping
    public List<Dictionary> getSavedWords() {
        return dictionaryService.getSavedWords();
    }

    @PutMapping
    public List<Dictionary> saveWord(
            @RequestParam String word,
            @RequestParam String translation
    ) {
        dictionaryService.saveWord(word, translation);
        return dictionaryService.getSavedWords();
    }

}
