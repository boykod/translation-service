package com.project.translateservice.dictionary.repository;

import com.project.translateservice.dictionary.model.Dictionary;
import java.util.List;

public interface DictionaryRepository {

    List<Dictionary> getSavedWords();

    void saveWord(String word, String translation);

}
