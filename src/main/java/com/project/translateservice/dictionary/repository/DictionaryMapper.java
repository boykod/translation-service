package com.project.translateservice.dictionary.repository;

import com.project.translateservice.dictionary.model.Dictionary;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class DictionaryMapper implements RowMapper<Dictionary> {

    @Override
    public Dictionary mapRow(ResultSet rs, int rowNum) throws SQLException {
        Dictionary dictionary = new Dictionary();

        dictionary.setId(rs.getInt("id"));
        dictionary.setWord(rs.getString("word"));
        dictionary.setTranslation(rs.getString("translation"));

        return dictionary;
    }
}
