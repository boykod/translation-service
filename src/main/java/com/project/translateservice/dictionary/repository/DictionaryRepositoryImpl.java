package com.project.translateservice.dictionary.repository;

import com.project.translateservice.dictionary.model.Dictionary;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class DictionaryRepositoryImpl implements DictionaryRepository {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final DictionaryMapper dictionaryMapper;

    @Autowired
    public DictionaryRepositoryImpl(NamedParameterJdbcTemplate jdbcTemplate, DictionaryMapper dictionaryMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.dictionaryMapper = dictionaryMapper;
    }

    @Override
    public List<Dictionary> getSavedWords() {
        String query = "SELECT * FROM dictionary";
        return jdbcTemplate.query(query, dictionaryMapper);
    }

    @Override
    public void saveWord(String word, String translation) {
        MapSqlParameterSource fields = getMap(word, translation);

        String query = "INSERT INTO dictionary (word, translation) VALUES (:word, :translation)";

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(query, fields, keyHolder);
    }

    @NotNull
    private MapSqlParameterSource getMap(@NotNull String word, @NotNull String translation) {
        MapSqlParameterSource dictionary = new MapSqlParameterSource();

        dictionary.addValue("word", word);
        dictionary.addValue("translation", translation);
        return dictionary;
    }
}
