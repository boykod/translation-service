package com.project.translateservice.translate.service;

import com.project.translateservice.translate.model.Translate;

public interface TranslateService {

    Translate translate(
            String sourceLanguage,
            String translateLanguage,
            String source,
            String email
    );

}
