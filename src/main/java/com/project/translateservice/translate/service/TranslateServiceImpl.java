package com.project.translateservice.translate.service;

import com.project.translateservice.translate.model.Translate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import static com.project.translateservice.utils.ConstantsKt.*;

@Service
public class TranslateServiceImpl implements TranslateService {

    private RestTemplate restTemplate = new RestTemplate();

    @Override
    public Translate translate(
            String sourceLanguage,
            String translateLanguage,
            String source,
            String email
    ) {
        //https://api.mymemory.translated.net/get?q=Hello World!&langpair=en|it&de=email@gmail.com
        String uri = SERVICE_URL + Q + source + AND + LANGPAIR + sourceLanguage + "|" + translateLanguage + AND + DE + email;
        return restTemplate.getForObject(uri, Translate.class);
    }

}
