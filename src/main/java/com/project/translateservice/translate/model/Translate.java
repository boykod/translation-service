package com.project.translateservice.translate.model;

public class Translate {

    private TranslateResponseData responseData;

    public Translate() {
    }

    public Translate(TranslateResponseData responseData) {
        this.responseData = responseData;
    }

    public TranslateResponseData getResponseData() {
        return responseData;
    }

    public void setResponseData(TranslateResponseData responseData) {
        this.responseData = responseData;
    }
}

