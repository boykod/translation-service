package com.project.translateservice.translate.model;

public class TranslateResponseData {

    private String translatedText;
    private Float match;

    public TranslateResponseData() {
    }

    public TranslateResponseData(String translatedText, Float match) {
        this.translatedText = translatedText;
        this.match = match;
    }

    public String getTranslatedText() {
        return translatedText;
    }

    public void setTranslatedText(String translatedText) {
        this.translatedText = translatedText;
    }

    public Float getMatch() {
        return match;
    }

    public void setMatch(Float match) {
        this.match = match;
    }
}
