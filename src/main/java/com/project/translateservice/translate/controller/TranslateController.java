package com.project.translateservice.translate.controller;

import com.project.translateservice.translate.model.Translate;
import com.project.translateservice.translate.service.TranslateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("translate")
public class TranslateController {

    private final TranslateService translateService;

    @Autowired
    public TranslateController(TranslateService translateService) {
        this.translateService = translateService;
    }

    @GetMapping
    public Translate translate(
            @RequestParam String sourceLanguage,
            @RequestParam String translateLanguage,
            @RequestParam String source,
            @RequestParam String email
    ) {
        return translateService.translate(sourceLanguage, translateLanguage, source, email);
    }

}
