package com.project.translateservice.utils

const val SERVICE_URL = "https://api.mymemory.translated.net/get?"
const val Q = "q="
const val LANGPAIR = "langpair="
const val DE = "de="
const val AND = "&"